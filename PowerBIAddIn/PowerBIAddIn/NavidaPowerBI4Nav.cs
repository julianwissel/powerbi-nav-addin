﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.IdentityModel.Clients.ActiveDirectory;
using System.Web.Script.Serialization;
using System.Net;
using System.Web;

using System.Windows.Forms;
using System.Drawing;
using System.IO;

namespace NavidaPowerBI4Nav
{


    public class NavidaPowerBI4Nav
    {
        //public static string getAccessTokenFromResfreshToken(string refreshToken)
        //{
        //    try
        //    {
        //        string authorityUri = "https://login.windows.net/common/oauth2/authorize/";
        //        string ClientID = "313f0deb-f3cc-4511-9044-617eaa3bc6c2";
        //        string ClientSecret = "poedNbQQN6yHRecWnGpL4fDKntZapWsSlp7ITmOkmFk=";

        //        TokenCache TC = new TokenCache();

        //        AuthenticationContext AC = new AuthenticationContext(authorityUri, TC);
        //        ClientCredential cc = new ClientCredential(ClientID, ClientSecret);


        //        AuthenticationResult authResult = AC.AcquireTokenByRefreshToken(refreshToken, cc);

        //        return authResult.AccessToken;
        //    }
        //    catch(Exception e)
        //    {
        //        return "";
        //    }
        //}
        public static string GetAccessToken(string resourceUri, string clientId, string redirectUri, string authUri)
        {
            AuthenticationResult result = null;
            try
            {
                Task.Run(async () =>
                {
                    TokenCache TC = new FileCache();
                    AuthenticationContext authContext = new AuthenticationContext(authUri, TC);
                    result = await authContext.AcquireTokenAsync(resourceUri, clientId,
                        new Uri(redirectUri),
                        new PlatformParameters(PromptBehavior.Auto));

                }).Wait();


                return result.AccessToken;
            }
            catch (Exception) { }
            return "Exception";
        }

        public static string GetAccessToken(string resourceUri, string clientId, string redirectUri)
        {
            return GetAccessToken(resourceUri, clientId, redirectUri,
                "https://login.windows.net/common/oauth2/authorize");
        }

        public static report[] GetReports(string groupID)
        {
            string powerBIApiUrl;
            if (groupID != "")  {
                powerBIApiUrl = String.Format("https://api.powerbi.com/v1.0/myorg/groups/{0}/reports", groupID);
            }
            else {
                powerBIApiUrl = "https://api.powerbi.com/v1.0/myorg/reports";
            }

            string token = GetAccessToken("https://analysis.windows.net/powerbi/api", "2ff97dc5-e6b5-4aef-80f0-b300d7f9fb8a", "https://login.live.com/oauth20_desktop.srf");
            //GET web request to list all reports.
            HttpWebRequest request = System.Net.WebRequest.Create(powerBIApiUrl) as System.Net.HttpWebRequest;
            request.KeepAlive = true;
            request.Method = "GET";
            request.ContentLength = 0;
            request.ContentType = "application/json";
            request.Headers.Add("Authorization", String.Format("Bearer {0}", token));

            //Get HttpWebResponse from GET request
            using (HttpWebResponse httpResponse = request.GetResponse() as System.Net.HttpWebResponse)
            {
                //Get StreamReader that holds the response stream
                using (StreamReader reader = new System.IO.StreamReader(httpResponse.GetResponseStream()))
                {
                    string responseContent = reader.ReadToEnd();

                    JavaScriptSerializer json = new JavaScriptSerializer();
                    Reports report = (Reports)json.Deserialize(responseContent, typeof(Reports));

                    return report.value;
                }
            }
        }
    }
    [Serializable]
    public class report
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
    [Serializable]
    public class Reports
    {
        public report[] value { get; set; }
    }



    class Caller
    {
        public static void Main(string[] args)
        {
            Console.Out.Write("neu:"+
            NavidaPowerBI4Nav.GetAccessToken("https://analysis.windows.net/powerbi/api", "2ff97dc5-e6b5-4aef-80f0-b300d7f9fb8a",
                "https://login.live.com/oauth20_desktop.srf"));
        }   
}



    //[ControlAddInExport("PowerBIControl")]
    //public class TestStuff : StringControlAddInBase
    //{
    //    static String getNewToken()
    //    {
    //        return "test test";
    //    }
    //    protected override Control CreateControl()
    //    {
    //        TextBox control = new TextBox();

    //        control.MinimumSize = new Size(50, 0);
    //        control.MaximumSize = new Size(500, Int32.MaxValue);

    //        control.BackColor = Color.LightBlue;
    //        control.Font = new Font("Tahoma", 9, FontStyle.Bold);
    //        control.DoubleClick += control_DoubleClick;

    //        return control;
    //    }
    //    /// Raises an event when the user double-clicks the text box.
    //    private void control_DoubleClick(object sender, EventArgs e)
    //    {
    //        int index = 0;
    //        string data = this.Control.Text;
    //        this.RaiseControlAddInEvent(index, data);

    //    }
    //}
}