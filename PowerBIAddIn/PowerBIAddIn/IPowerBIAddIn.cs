﻿
using Microsoft.Dynamics.Framework.UI.Extensibility;

namespace NavidaPowerBI4Nav
{
    [ControlAddInExport("PowerBIControl")]
    public interface IPowerBIAddIn
    {
        [ApplicationVisible]
        event ApplicationEventHandler ControlAddInReady;
        [ApplicationVisible]
        event ApplicationEventHandler FrameLoaded;
        [ApplicationVisible]
        void InitializeReport(string ReportID, string AccessToken, string FilterField, string FilterValue);
        [ApplicationVisible]
        void SwitchReport(string ReportID, string AccessToken, string FilterField, string FilterValue);
        [ApplicationVisible]
        void SetFilter(string AccessToken, string FilterField, string FilterValue);
        [ApplicationVisible]
        void TestTest2(string AccessToken, string FilterField, string FilterValue);
    }
}
