var viz = null;
        // get the access token.
 
var DefaultAccessToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsIng1dCI6Ik1uQ19WWmNBVGZNNXBPWWlKSE1iYTlnb0VLWSIsImtpZCI6Ik1uQ19WWmNBVGZNNXBPWWlKSE1iYTlnb0VLWSJ9.eyJhdWQiOiJodHRwczovL2FuYWx5c2lzLndpbmRvd3MubmV0L3Bvd2VyYmkvYXBpIiwiaXNzIjoiaHR0cHM6Ly9zdHMud2luZG93cy5uZXQvY2ZkZDJlOGUtNjRkZS00MDE2LWFlYzQtZTk1NDY5NTUzYjFhLyIsImlhdCI6MTQ2MDcxNDQ4NSwibmJmIjoxNDYwNzE0NDg1LCJleHAiOjE0NjA3MTgzODUsImFjciI6IjEiLCJhbXIiOlsicHdkIl0sImFwcGlkIjoiMzEzZjBkZWItZjNjYy00NTExLTkwNDQtNjE3ZWFhM2JjNmMyIiwiYXBwaWRhY3IiOiIxIiwiZmFtaWx5X25hbWUiOiJMw7xlcnMiLCJnaXZlbl9uYW1lIjoiQmrDtnJuIiwiaXBhZGRyIjoiNzkuMjQ0LjIyLjE2NSIsIm5hbWUiOiJCasO2cm4gTMO8ZXJzIiwib2lkIjoiMDIxMTVjOWQtN2ZmMy00Zjc4LWJkM2MtODU4NjUxNTBiMGY3IiwicHVpZCI6IjEwMDMwMDAwODk3RjJBN0UiLCJzY3AiOiJEYXNoYm9hcmQuUmVhZC5BbGwgRGF0YXNldC5SZWFkV3JpdGUuQWxsIEdyb3VwLlJlYWQgUmVwb3J0LlJlYWQuQWxsIiwic3ViIjoieFRRWGZuQ2pBR0VFX1c1UGFmbVB5SkZXVVVpSU1fNC1tMW9TYWN6LVh4OCIsInRpZCI6ImNmZGQyZThlLTY0ZGUtNDAxNi1hZWM0LWU5NTQ2OTU1M2IxYSIsInVuaXF1ZV9uYW1lIjoiYmpvZXJuLmx1ZWVyc0BuYXZpZGEuZXUiLCJ1cG4iOiJiam9lcm4ubHVlZXJzQG5hdmlkYS5ldSIsInZlciI6IjEuMCJ9.aEQzvYMcp4VTmblZRKxXwwaV1BI5eR_tVzonxPplxJCVcaAOYg7CU-Lfv4VaRbJbGjeecDIvpYvzi8vaSVTghyab1dAcbwmCiHWGsbi7gQaS5PJuibRecluZGpBd8Tp60MR8DK9OexydDAY4y0whj8KXkMu1dKz5OnNKTfmYxGeR6DJA4ZADqvagKhPjbD_7miLMs72xmbPl2L05cwaZrTtQrpgZgq8u2VJPDpLJxfZGrAcmPsa_p216c4FD3PtlsjS0ng7a88bPWeXGt0-q0zQSxV5TmJ6yXqXPIhCI_dhVUxtCj0zAv_8oBFxdGVzIPRSimKkisaHvA-dF-_YUtA";
var UserAccessToken = "";
var iframeID = "iFrameEmbedReport";
 
 function InitializeFrame(controlId) {

			var placeholderDiv = document.getElementById(controlId);
			
			
			//------debug kram		
			var infoDiv = 	document.createElement("div"); 	
			infoDiv.id = "infoDiv";
			placeholderDiv.appendChild(infoDiv);			
			
			debugMessage("InitializeMap");

			//------ende debug kram		
			
			
			ifrm = document.createElement("IFRAME");
			ifrm.id  = iframeID;
			ifrm.setAttribute("src", "");
			ifrm.style.width = 1200+"px";
			ifrm.style.height = 800+"px";
			placeholderDiv.appendChild(ifrm);
		  

            // listen for message to receive message from embedded report.
            if (window.addEventListener) {
                window.addEventListener("message", receiveMessage, false);
            } else {
                window.attachEvent("onmessage", receiveMessage);
            }

			Microsoft.Dynamics.NAV.InvokeExtensibilityMethod ('FrameLoaded', null);
		

        }

        function InitializeReport(ReportID,  AccessToken,  FilterField, FilterValue){


			debugMessage("report: " + ReportID + FilterField);

			UserAccessToken = AccessToken;

			ifrm = document.getElementById(iframeID);	
		/*	ifrm.setAttribute("src", embedURL + FilterString);*/
			//valid url mit filter https://app.powerbi.com/reportEmbed?reportId=c03d1a44-f580-473c-96e5-a613575c061e&$filter=Produkt/Produktart eq 'Anlage'      encode filter: ..$filter=Produkt/Produktart%20eq%20%27Anlage%27
			//https://app.powerbi.com/reportEmbed?reportId=c7e61e33-81db-4440-9ac3-3ea1f3068207&groupId=085d9299-0677-4542-8947-cf28b36e6a4b&$filter=Details/Kunde_ID_RG eq 'M0_112000'
			
			embedURL = "https://app.powerbi.com/reportEmbed?reportId=" + ReportID;
			if(FilterField!=""){
				embedURL = embedURL + "&$filter=" + FilterField + "%20eq%20%27" + FilterValue + "%27";
			}

			debugMessage("url: " + ReportID + FilterField);
			
			ifrm.setAttribute("src", embedURL);
			ifrm.onload = postActionLoadReport;

        }
		function debugMessage(msg){
/*			var p = document.createElement('p');
			p.textContent =msg;
			document.getElementById("infoDiv").appendChild(p); */
		}



        function SwitchReport(ReportID,  AccessToken,  FilterField, FilterValue){
		
			if(AccessToken!=""){
				UserAccessToken = AccessToken;
			}
			message = "";
			if(FilterField!=""){
				  filterString = FilterField + " eq '" + FilterValue +"'";
				  var m = 
				  {
					  action: "loadReport",
					  reportId: ReportID,
					  accessToken: UserAccessToken,
					  oDataFilter: filterString
				  };
				message = JSON.stringify(m);
			}else{
				  var m = 
				  {
					  action: "loadReport",
					  reportId: ReportID,
					  accessToken: AccessToken
				  };
				message = JSON.stringify(m);				
			}
            // push the message.
            iframe = document.getElementById(iframeID);
            iframe.contentWindow.postMessage(message, "*");
        }

        function SetFilter(AccessToken,  FilterField, FilterValue){
			debugMessage(FilterField + FilterValue);
		
			if(AccessToken!=""){
				UserAccessToken = AccessToken;
			}
			filterString = FilterField + " eq '" + FilterValue + "'";
			  var m = 
			  {
				  action: "loadReport",
				  accessToken: UserAccessToken,
				  oDataFilter: filterString
			  };
			message = JSON.stringify(m);
            // push the message.
            iframe = document.getElementById(iframeID);
			 iframe.contentWindow.postMessage(message, "*");
        }
		
		
        // The embedded report posts message for errors to parent window.  listen and handle as appropriate
        function receiveMessage(event)
        {

            if (event.data) {
                try {
                    messageData = JSON.parse(event.data);
                    if (messageData.event === "reportPageLoaded")
                    {
                    }
                }
                catch (e) {
                    // do something smart
                }
            }
        }

        
        // post the auth token to the iFrame. 
        function postActionLoadReport() {

            // construct the push message structure
            // this structure also supports setting the reportId, groupId, height, and width.
            // when using a report in a group, you must provide the groupId on the iFrame SRC
            var m = { action: "loadReport", accessToken: UserAccessToken};
            message = JSON.stringify(m);

            // push the message.
            iframe = document.getElementById(iframeID);
            iframe.contentWindow.postMessage(message, "*");
        }
		
		